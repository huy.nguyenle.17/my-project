﻿
using Microsoft.Extensions.DependencyInjection;
using StoreAPI.Infrastructures;
using StoreAPI.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api.Infrastructures
{
    public class DIRepositoryConfig
    {
        public static void DIConfigs(IServiceCollection services)
        {
            services.AddScoped<IDatabaseFactory, DatabaseFactory>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
        }
    }
}

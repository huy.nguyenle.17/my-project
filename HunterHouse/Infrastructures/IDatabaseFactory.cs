﻿using api.Data;
using api.Infrastructures;
using HunterHouse.DataContext;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StoreAPI.Infrastructures
{
    public interface IDatabaseFactory
    {
        HunterHouseDbContext Init();
    }
    public class DatabaseFactory : Disposable, IDatabaseFactory
    {
        HunterHouseDbContext dbContext;
        public HunterHouseDbContext Init()
        {
            return dbContext ?? (dbContext = new HunterHouseDbContext());
        }
        protected override void DisposeCore()
        {
            if (dbContext != null)
                dbContext.Dispose();
        }
    }
}

﻿using api.Data;
using HunterHouse.DataContext;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace StoreAPI.Infrastructures
{
    public abstract class RepositoryBase<T> where T: class
    {
        #region Properties
        private HunterHouseDbContext dbContext;
        private readonly DbSet<T> dbSet;

        protected IDatabaseFactory DbFactory
        {
            get;
            private set;
        }

        protected HunterHouseDbContext DbContext
        {
            get { return dbContext ?? (dbContext = DbFactory.Init()); }
        }
        #endregion
        protected RepositoryBase(IDatabaseFactory dbFactory)
        {
            DbFactory = dbFactory;
            dbSet = DbContext.Set<T>();
        }
        #region Implementation
        public virtual void Add(T entity)
        {
            dbSet.Add(entity);
        }  
        public virtual void Update(T entity)
        {

            dbSet.Attach(entity);
            dbContext.Entry(entity).State = EntityState.Modified;
        }

        public virtual void Delete(T entity)
        {
            dbSet.Remove(entity);
        }

        public virtual T GetById(int id)
        {
            return dbSet.Find(id);
        }

        public virtual IEnumerable<T> GetAll()
        {
            return dbSet;
        }
        #endregion
    }
}

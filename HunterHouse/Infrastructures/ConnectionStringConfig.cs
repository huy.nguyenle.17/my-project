﻿using api.Data;
using HunterHouse.DataContext;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StoreAPI.Infrastructures
{
    public class ConnectionStringConfig
    {
        public static void Config(IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<HunterHouseDbContext>(option =>
            {
                option.UseSqlServer(configuration.GetConnectionString("HunterHouseApiDb"));
            });
        }
    }
}

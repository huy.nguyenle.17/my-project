﻿
using HunterHouse.DataContext;
using StoreAPI.Infrastructures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api.Infrastructures
{
    public interface IUnitOfWork
    {
        void Commit();
    }
    public class UnitOfWork : IUnitOfWork
    {
        private HunterHouseDbContext _dbContext;
        private readonly IDatabaseFactory _dbFactory;
        public UnitOfWork(IDatabaseFactory dbFactory)
        {
            _dbFactory = dbFactory;
        }
        public HunterHouseDbContext DbContext
        {
            get { return _dbContext ?? (_dbContext = _dbFactory.Init()); }
        }

        public void Commit()
        {
            DbContext.SaveChanges();
        }
    }
}

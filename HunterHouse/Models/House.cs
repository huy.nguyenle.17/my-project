﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HunterHouse.Models
{
    public class House
    {
        public string HouseId { get; set; }
        public string LocationId { get; set; }
        public string SellerId { get; set; }
        public string TenantId { get; set; }
        public string UserId { get; set; }
        public string HouseSpecId { get; set; }
        public Decimal Price { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }

    }
}

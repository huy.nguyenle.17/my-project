﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HunterHouse.Models
{
    public class Location
    {
        public string LocationId { get; set; }
        public string CityId { get; set; }
        public string DistrictId { get; set; }
        public string Address { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HunterHouse.Models
{
    public class SellerTenant
    {
        public int SellerTenantId { get; set; }
        public string UserId { get; set; }
        public string TenantId { get; set; }

        //Relational
        public User User { get; set; }
        public Tenant Tenant { get; set; }
    }
}

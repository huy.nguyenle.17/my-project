﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HunterHouse.Models
{
    public class HouseSpec
    {
        public string HouseSpecId { get; set; }
        public string Description { get; set; }
        public int NumBedRoom { get; set; }
        public int NumToilet { get; set; }
        public bool HasFullFurniture { get; set; }
        public bool HasAirConditioner { get; set; }
        public bool HasFrigde { get; set; }
        public bool HasWashingMachine { get; set; }
        public float Area { get; set; }
        public int Floor { get; set; }
        public string TypeHouse { get; set; }
    }
}

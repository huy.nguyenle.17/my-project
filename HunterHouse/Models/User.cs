﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HunterHouse.Models
{
    public class User
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string FullName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string LocationId { get; set; }
        public string RoleId { get; set; }

        //Relational
        public Role Role { get; set; }
        public Location Location { get; set; }
        public ICollection<House> Houses { get; set; }
        public ICollection<SellerTenant> SellerTenants { get; set; }
    }
}
